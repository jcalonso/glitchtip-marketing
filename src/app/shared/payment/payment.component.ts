import { Component } from '@angular/core';
import { environment } from '../../../environments/environment';
import { LinksService } from 'src/app/links.service';
/**
 * Copied HTML from the frontend version of this, pulled some data that is
 * currently in use and hardcoded here, and sharing the SCSS file
 */
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: [
    '../../../../glitchtip-frontend/src/app/settings/subscription/payment/payment.component.scss',
    './payment.component.scss',
  ],
})
export class PaymentComponent {
  billingEmail = environment.billingEmail;
  registerLink = this.links.registerLink;

  constructor(private links: LinksService) {}

  /**
   * Pulled from frontend's payment component's planOptions$, pruned to use
   * only what's needed
   */
  planOptions = [
    {
      name: 'Free',
      description: 'Up to 1000 events per month',
      plans: [{ amount: 0 }],
    },
    {
      name: 'Small',
      description: 'Up to 100k events per month',
      plans: [{ amount: 15 }],
    },
    {
      name: 'Medium',
      description: 'Up to 500k events per month',
      plans: [{ amount: 50 }],
    },
    {
      name: 'Large',
      description: 'Up to 3 million events per month',
      plans: [{ amount: 250 }],
    },
  ];
}
