---
title: Installation Guide
publish: true
---

# GlitchTip Installation Guide

GlitchTip can be run with Docker. We recommend Docker Compose, DigitalOcean App Platform, or Heroku for smaller installations.

## Docker Compose

Docker Compose is a simple way to run GlitchTip on a single server.

1. Install Docker and Docker Compose. On Debian/Ubuntu this is `sudo apt install docker-compose docker.io`
2. Copy [docker-compose.sample.yml](/assets/docker-compose.sample.yml) to your server as `docker-compose.yml`.
3. Edit the environment section of docker-compose.yml. See the Configuration section below.
4. Start docker service `docker-compose up -d` now the service should be running on port 8000.

It's highly recommended to configure SSL next. Use nginx or preferred solution.

### Recommended nginx and SSL solution

- Install nginx. Ex: `sudo apt install nginx`.
- (on Debian/Ubuntu) edit `/etc/nginx/sites-enabled/default` for example:

```
server {
    server_name glitchtip.example.com;
    access_log  /var/log/nginx/access.log;

    location / {
        proxy_pass http://127.0.0.1:8000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}
```

This configuration will direct glitchtip.example.com to port 8000 (the default GlitchTip docker compose port).

- Install and run certbot. Follow [instructions](https://certbot.eff.org/instructions).

### Upgrading

1. Pull latest docker image `docker-compose pull`
1. Restart `docker-compose stop` and `docker-compose up -d`

Database migrations will automatically happen.

## DigitalOcean App Platform

Get started by clicking here. Note this is a referral link and is a great way to help fund GlitchTip.

<a href="https://cloud.digitalocean.com/apps/new?repo=https://gitlab.com/glitchtip/glitchtip/tree/master&refcode=7e90b8fb37f8">
<img src="https://www.deploytodo.com/do-btn-blue.svg" alt="Deploy to DigitalOcean" style="width:250px;"/>
</a>

Leave environment variables blank and click next. Pick the basic or pro plan. One 512 MB RAM | 1 vCPU is fine to start with. Click Launch. Now copy [app-platform.yaml](https://gitlab.com/glitchtip/glitchtip/-/blob/master/app-platform.yaml) to your local computer. Edit the following

### Name and region

This can be anything. We default to "glitchtip" and "nyc".

### Environment Variables

At a minimum, set the SECRET_KEY to a random string of letters.

See [Configuration](https://glitchtip.com/documentation/install#Configuration) for more information.

### Redis

GlitchTip requires Redis for sending notification, managing events, and more. Go to https://cloud.digitalocean.com/databases/ and create a new redis database. For almost all size instances, the 1 GB RAM | 1 vCPU instance is sufficient. Enter your redis database's name in the glitchtip-redis section. Let's assume it's named "glitchtip-redis". Both "name" and "cluster_name" must be the same value.

```
- name: glitchtip-redis
  engine: REDIS
  production: true
  cluster_name: glitchtip-redis
```

Ensure the environment variable "REDIS_URL" uses the same name. If you didn't name your redis instance "glitchtip-redis" then make sure to update it.

### Deploying

You'll need to install [doctl](https://www.digitalocean.com/docs/apis-clis/doctl/how-to/install/) and log in.

Run `doctl apps list` to get your app's id.

Now apply your app-platform.yaml spec with `doctl apps update 11111111-1111-1111-1111-111111111 --spec app-platform.yaml` (enter your actual id)

After deployment, you should be able to visit the app URL and start using GlitchTip!

### Production considerations

If you intend to use GlitchTip in production, consider upgrading your Postgres database to a production instance. In the web interface, go to Manage Components, glitchtip-db, Upgrade to a managed database.

If you haven't already, you'll need to set up email via environment variables.

### Upgrading GlitchTip

By default, the docker image tag is "latest". Click Deploy to upgrade to the latest GlitchTip docker image.

## Heroku

<a href="https://heroku.com/deploy?template=https://github.com/burke-software/GlitchTip">
<img src="https://www.herokucdn.com/deploy/button.svg" alt="Deploy to Heroku" style="width:200px;"/>
</a>

1. Deploy the app by clicking the above link and following the prompt.
2. SECRET_KEY is generated for you. Other environment variables must be entered in the app's Settings, Config Vars. See configuration [docs](https://glitchtip.com/documentation/install#Configuration).

### Production considerations

Consider upgrading your Postgres and web dyno plan for production usage.

Most users do not need additional workers. However if you do, create a third dyno typed called extra_worker. Set the run command to `./bin/run-celery.sh`. Do not increase the "worker" dyno count because this these run with an embedded Celery beat scheduler.

### Upgrading GlitchTip

By default, the docker image tag is "latest". Click Deploy to upgrade to the latest GlitchTip docker image.

## Helm

Installing GlitchTip with Helm for Kubernetes is a good option for high throughput sites and users who are very comfortable using Kubernetes.
This method is recommended only for users who are very comfortable managing Docker and Kubernetes.

app.glitchtip.com uses this method with a managed DigitalOcean database.

1. Add our Helm chart repo `helm repo add glitchtip https://glitchtip.gitlab.io/glitchtip-helm-chart/`
2. Review our [values.yaml](https://gitlab.com/glitchtip/glitchtip-helm-chart/-/blob/master/values.yaml). At a minimum you'll need to set databaseURL, secretKey, and possibly image.tag.
3. Install the chart `helm install glitchtip/glitchtip --set databaseURL=your_db --set secretKey=random_string` (you may want to use a values.yaml file instead of --set)

Our chart is published [here](https://gitlab.com/glitchtip/glitchtip-helm-chart). We only publish chart updates when the helm configuration changes. For regular GlitchTip updates, set the image.tag value to the version you want on [dockerhub](https://hub.docker.com/r/glitchtip/glitchtip/tags). Leaving the tag as "latest" is not recommended for production use cases as you may get "surprise" updates. Please carefully review value changes on each update. We recommend using [Helm Diff](https://github.com/databus23/helm-diff).

For high availability, production servers we recommend using multiple Kubernetes Nodes, an ingress and/or load balancer, a pod disruption budget, anti-affinity, and a managed PostgreSQL high availability database.

# Configuration

Required environment variables:

- `SECRET_KEY` set to any random string
- Set up email:
- `EMAIL_URL`: SMTP string. It will look something like `"smtp://email@:password@smtp_url:port"`. See format examples [here](https://django-environ.readthedocs.io/en/latest/#supported-types)
- Alternatively, use the Mailgun API by setting `MAILGUN_API_KEY` and `MAILGUN_SENDER_DOMAIN`. Set `EMAIL_BACKEND` to `anymail.backends.mailgun.EmailBackend`
- `DEFAULT_FROM_EMAIL` Default from email address. Example `info@example.com`
- `GLITCHTIP_DOMAIN` Set to your domain. Include scheme (http or https). Example: `https://glitchtip.example.com`.

Optional environment variables:

- `I_PAID_FOR_GLITCHTIP` [Donate](https://liberapay.com/GlitchTip/donate), set this to "true", and some neat things will happen. This won't enable extra features but it will enable our team to continue building GlitchTip. We pay programmers, designers, illustrators, and free tier hosting on app.glitchtip.com without venture capital. We ask that all self-host users pitch in with a suggested donation of $5 per month per user. Prefer an invoice and support instead? Business users can also consider a paid support plan. Reach out to us at sales@glitchtip.com. Contributors on [Gitlab](https://gitlab.com/glitchtip) should also enable this.
- `GLITCHTIP_MAX_EVENT_LIFE_DAYS` (Default 90) Events and associated data older than this will be deleted from the database
- `REDIS_URL` Set redis host explicitly. Example: `redis://:password@host:port/database`. You may also set them separately with `REDIS_HOST`, `REDIS_PORT`, `REDIS_DATABASE`, and `REDIS_PASSWORD`.
- `DATABASE_URL` Set PostgreSQL connect string. PostgreSQL 11 and above are supported.
- Content Security Policy (CSP) headers are enabled by default. In most cases there is no need to change these. However you may add environment variables as documented in [django-csp](https://django-csp.readthedocs.io/en/latest/configuration.html#policy-settings) to modify them. For example, set `CSP_DEFAULT_SRC='self',scripts.example.com` to modify the default CSP header. Note the usage of comma separated values and single quotes on certain values such as 'self'.
- `ENABLE_OPEN_USER_REGISTRATION` (Default False) Set to True to allow any user to register. When False, user self sign up is disabled after the first organization is created. This setting may be useful for SaaS services or self hosting that encourage peers to sign themselves up.

### File storage

Media storage is limited and optional at this time. Storage is necessary to enable file uploads, such as sourcemaps. GlitchTip can support both local storage and remote storage via [django-storages](https://django-storages.readthedocs.io/en/latest/).

Example AWS S3 or DigitalOcean Spaces.

- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_STORAGE_BUCKET_NAME
- AWS_S3_ENDPOINT_URL - Necessary if using DigitalOcean Spaces. Set to `https://<your-region>.digitaloceanspaces.com`

More generally, GlitchTip maps environment variables to django-storages options. If you find that your provider is supported by django-storages but not GlitchTip, please submit a [merge request](https://gitlab.com/glitchtip/glitchtip-backend/-/merge_requests) that adds more environment variable mapping.

For local storage with Docker, you may use a volume. Refer to Kubernetes or Docker Compose documentation on creating volumes. In the future, docker-compose examples with volumes will be provided by default.

### Search Language

GlitchTip uses PostgreSQL full-text search. It will use the default PostgreSQL "text_search_config". In most cases there is no need to modify this. However, you may wish to change it as described [here](https://www.postgresql.org/docs/13/textsearch-configuration.html). This only affects search terms, it does not affect the site language. For example, if your preferred reading language is French and your code and user base uses English, you should pick English.

## Django Admin

Django Admin is not necessary for most users. However if you'd like the ability to fully manage users beyond what our frontend offers, it may be useful. To enable, create a super user via the Django command

`./manage.py createsuperuser`

Then go to `/admin/` and log in.

### Social Authentication (OAuth)

You may add Social Accounts in Django Admin at `/admin/socialaccount/socialapp/`. GlitchTip supports Gitlab, Google, Microsoft, and Github OAuth. If you are not familiar with OAuth, please refer to django-allauth's [providers documentation](https://django-allauth.readthedocs.io/en/latest/providers.html). If your OAuth provider is supported by django-allauth but not GlitchTip, please open a merge request to add both back-end (typically env var mapping) and front-end (logo, redirects, etc) support.
